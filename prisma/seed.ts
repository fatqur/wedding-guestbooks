import { PrismaClient } from '@prisma/client';
import { roles, keymaps } from './data/roles';
import { users } from './data/users';
import { hashSync } from 'bcrypt';
import { guestbooks } from './data/guestbooks';

const prisma = new PrismaClient();

async function main() {
  await seedRoles();
  await seedUsers();
  await seedGuestBooks();
}

function titleCase(str: string) {
  str = str.replace('_', ' ');
  var splitStr = str.toLowerCase().split(' ');
  for (var i = 0; i < splitStr.length; i++) {
    splitStr[i] =
      splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  return splitStr.join(' ');
}

async function seedRoles() {
  for (const role of roles) {
    const permissions = role.permissions;
    let permcodes: { code: string }[] = [];
    for (const permission of permissions) {
      const module = permission[0].toLowerCase();
      const perms = permission[1].split(',');
      for (const act of perms) {
        const action = keymaps.filter((keymap) => keymap.action === act)[0];
        const code = `${action.desc}_${module}`;
        await prisma.permission.upsert({
          where: { code },
          update: {},
          create: {
            code,
            module: titleCase(module),
            name: action.desc,
          },
        });
        permcodes.push({ code });
      }
    }
    await prisma.role.upsert({
      where: { code: role.code },
      create: {
        code: role.code,
        name: titleCase(role.code),
        permissions: { connect: permcodes },
      },
      update: {
        permissions: {
          connect: permcodes,
        },
      },
    });
  }
}

async function seedUsers() {
  for (const user of users) {
    await prisma.user.upsert({
      where: { email: user.email },
      update: {
        name: user.name,
        password: hashSync(user.password, 12),
        roles: { connect: user.roles },
      },
      create: {
        name: user.name,
        email: user.email,
        password: hashSync(user.password, 12),
        roles: { connect: user.roles },
      },
    });
  }
}

async function seedGuestBooks() {
  await prisma.guestbook.createMany({ data: guestbooks });
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(() => {
    prisma.$disconnect();
  });
