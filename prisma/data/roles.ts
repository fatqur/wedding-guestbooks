const keymaps = [
  { action: 'b', desc: 'browse' },
  { action: 'r', desc: 'read' },
  { action: 'e', desc: 'edit' },
  { action: 'a', desc: 'add' },
  { action: 'd', desc: 'delete' },
  { action: 'ap', desc: 'approve' },
  { action: 'rs', desc: 'restore' },
  { action: 'dt', desc: 'destroy' },
];

const roles = [
  {
    code: 'superadmin',
    permissions: [
      ['users', 'b,r,e,a,d'],
      ['roles', 'b,r,e,a,d'],
      ['guestbooks', 'b,r,e,a,d'],
    ],
  },
];
export { keymaps, roles };
