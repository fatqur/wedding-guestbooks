export const guestbooks = [
  {
    name: 'John Smith',
    address: 'Jalan Sudirman No. 12, Jakarta',
    phone: '6281234567890',
    note: 'Wishing you a lifetime of love and happiness together!',
  },
  {
    name: 'Sarah Lee',
    address: 'Jalan Asia Afrika No. 45, Bandung',
    phone: '6282345678901',
    note: 'May your love continue to grow stronger with each passing day!',
  },
  {
    name: 'Michael Tan',
    address: 'Jalan M.H. Thamrin No. 67, Jakarta',
    phone: '6283456789012',
    note: 'Congratulations on finding your soulmate! Wishing you a wonderful life together.',
  },
  {
    name: 'Amanda Wong',
    address: 'Jalan Gajah Mada No. 89, Surabaya',
    phone: '6284567890123',
    note: 'May your marriage be filled with love, laughter, and joy!',
  },
  {
    name: 'David Lim',
    address: 'Jalan Diponegoro No. 23, Yogyakarta',
    phone: '6285678901234',
    note: 'Wishing you a lifetime of love, happiness, and adventure together!',
  },
  {
    name: 'Emily Chen',
    address: 'Jalan Merdeka No. 56, Bandung',
    phone: '6286789012345',
    note: 'Congratulations on your wedding! May your love story be as beautiful and magical as your special day.',
  },
  {
    name: 'William Tan',
    address: 'Jalan Pemuda No. 78, Surabaya',
    phone: '087890123456',
    note: 'Wishing you all the love and happiness in the world on your special day and always!',
  },
  {
    name: 'Jane Tjio',
    address: 'Jalan Asia Afrika No. 67, Jakarta',
    phone: '6288901234567',
    note: 'Congratulations on your wedding! May your love shine brighter with each passing day.',
  },
  {
    name: 'Thomas Lim',
    address: 'Jalan Gatot Subroto No. 34, Semarang',
    phone: '6289012345678',
    note: 'May your marriage be filled with laughter, love, and lots of happy memories!',
  },
  {
    name: 'Christina Ang',
    address: 'Jalan Thamrin No. 56, Medan',
    phone: '6290123456789',
    note: 'Congratulations on your wedding day! Wishing you a lifetime of love and happiness together.',
  },
];
