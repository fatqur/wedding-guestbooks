import { check, CustomValidator } from 'express-validator';
import NewUserNotification from '../notifications/NewUserNotification';
import UserService from '../services/UserService';
// import Crypt from '../utils/Crypt';

// const userActive: CustomValidator = async (value: string, { req }) => {
//   const user = await UserService.findUserByEmail(value);
//   if (user) {
//     if(user.status === 'blocked'){
//       return Promise.reject(req.t('auth.blocked'));
//     } else if (user.status === 'inactive') {
//       return Promise.reject(req.t('auth.inactive'));
//     } else if(user.status === 'unverified'){
//       return Promise.reject(req.t('auth.unverified'));
//     }
//   }
// };

// const accountTokenValid: CustomValidator = async (value, { req }) => {
//   const result = await UserService.findToken(req.body.email, value);
//   if (result) {
//     const jsontoken = Crypt.decrypt(value);
//     const token = JSON.parse(jsontoken);
//     let createdAt = result.createdAt;
//     const expiredDateTime = DateTime.fromJSDate(createdAt).plus({
//       minutes: token.expired,
//     });

//     if (expiredDateTime < DateTime.now()) {
//       const token = await UserService.getVerifyToken(req.body.email);
//       if (token) {
//         new NewUserNotification(req.body.email, token);
//       }
//       return Promise.reject(req.t('auth.verify_token_expired'));
//     }
//   } else {
//     return Promise.reject(req.t('auth.verify_token_invalid'));
//   }
// };

// const emailTokenValid: CustomValidator = async (value, { req }) => {
//   const result = await UserService.findTokenEmail(req.body.email, value);
//   if (result) {
//     const jsontoken = Crypt.decrypt(value);
//     const token = JSON.parse(jsontoken);
//     let createdAt = result.createdAt;
//     const expiredDateTime = DateTime.fromJSDate(createdAt).plus({
//       minutes: token.expired,
//     });

//     if (expiredDateTime < DateTime.now()) {
//       return Promise.reject(req.t('auth.email.token_expired'));
//     }
//   } else {
//     return Promise.reject(req.t('auth.email.token_invalid'));
//   }
// };

const loginValidation = [
  check('email')
    .trim()
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail()
    .isEmail()
    .withMessage((value, { req, path }) => {
      return req.t('validator.email', { field: path });
    }),

  check('password')
    .trim()
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    }),
];

const accountVerifyValidation = [
  check('email')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail()
    .normalizeEmail()
    .isEmail()
    .withMessage((value, { req, path }) => {
      return req.t('validator.email', { field: path });
    }),

  check('token')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    }),
  // .custom(accountTokenValid),
];

const changeEmailVerifyValidation = [
  check('email')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail()
    .normalizeEmail()
    .isEmail()
    .withMessage((value, { req, path }) => {
      return req.t('validator.email', { field: path });
    }),

  check('token')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    }),
  // .custom(emailTokenValid),
];

const setTokenValidation = [
  check('token')
    .trim()
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    }),
];

export {
  loginValidation,
  accountVerifyValidation,
  changeEmailVerifyValidation,
  setTokenValidation,
};
