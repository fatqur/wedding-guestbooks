import { check } from 'express-validator';
const createGuestbookValidation = [
  check('name')
    .trim()
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    }),
  check('address')
    .trim()
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    }),
  check('phone')
    .trim()
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    }),
  check('note')
    .trim()
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    }),
];

export { createGuestbookValidation };
