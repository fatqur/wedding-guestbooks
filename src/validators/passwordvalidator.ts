import { check, CustomValidator } from 'express-validator';
import { DateTime } from 'luxon';
import config from '../config';
// import OtpResetService from '../services/OtpResetService';
// import PasswordService from '../services/PasswordService';
import UserService from '../services/UserService';
// import Crypt from '../utils/Crypt';

const userExist: CustomValidator = async (value: string, { req }) => {
  const isExist = await UserService.isExist(value);
  if (!isExist) {
    return Promise.reject(req.t('auth.email_not_found'));
  }
};

const userActive: CustomValidator = async (value: string, { req }) => {
  const user = await UserService.findUserByEmail(value);
  if (user) {
    if (user.status == 'unverified') {
      return Promise.reject(req.t('auth.unverified'));
    } else if (user.status == 'inactive') {
      return Promise.reject(req.t('auth.inactive'));
    }
  } else {
    return Promise.reject(req.t('auth.email_not_found'));
  }
};

// const currentPassword: CustomValidator = async (value: string, { req }) => {
//   const userId = req.app.locals.user.id;
//   const valid = await PasswordService.checkPassword(userId, value);
//   if (!valid) {
//     return Promise.reject(req.t('password.incorrect'));
//   }
// };

// const cannotSameCurrentPassword: CustomValidator = async (
//   value: string,
//   { req }
// ) => {
//   const userId = req.app.locals.user.id;
//   const same = await PasswordService.checkPassword(userId, value);
//   if (same) {
//     return Promise.reject(req.t('password.cannot_same'));
//   }
// };

const passwordRule: CustomValidator = async (value: string, { req }) => {
  let validated = true;
  // min length = 8
  if (value.length < 8) {
    validated = false;
  }

  // required min 1 lowercase
  if ((value.match(/[a-z]/g) || []).length < 1) {
    validated = false;
  }
  // required min 1 uppercase
  if ((value.match(/[A-Z]/g) || []).length < 1) {
    validated = false;
  }
  // required min 1 number
  if ((value.match(/[0-9]/g) || []).length < 1) {
    validated = false;
  }
  // required min 1 special char
  if ((value.match(/[^a-zA-Z0-9]/g) || []).length < 1) {
    validated = false;
  }

  if (!validated) {
    return Promise.reject(req.t('password.validation'));
  }
};

const passwordConfirmation: CustomValidator = async (value, { req }) => {
  if (value !== req.body.password) {
    return Promise.reject(req.t('password.confirmation'));
  }
};

// const resetTokenValid: CustomValidator = async (value, { req }) => {
//   const result = await PasswordService.findToken(req.body.email, value);
//   if (result) {
//     const jsontoken = Crypt.decrypt(value);
//     const token = JSON.parse(jsontoken);
//     let createdAt = result.createdAt;
//     const expiredDateTime = DateTime.fromJSDate(createdAt).plus({
//       minutes: token.expired,
//     });

//     if (expiredDateTime < DateTime.now()) {
//       return Promise.reject(req.t('password.token_expired'));
//     }
//   } else {
//     return Promise.reject(req.t('password.token_invalid'));
//   }
// };

// const resetOtpValid: CustomValidator = async (value, { req }) => {
//   const result = await OtpResetService.findOtp(req.body.email, value);
//   if (result) {
//     let createdAt = result.createdAt;
//     const expiredDateTime = DateTime.fromJSDate(createdAt).plus({
//       minutes: config.expiration.reset_password,
//     });

//     if (expiredDateTime < DateTime.now()) {
//       return Promise.reject(req.t('password.token_expired'));
//     }
//   } else {
//     return Promise.reject(req.t('password.token_invalid'));
//   }
// };

const emailNotifValidation = [
  check('email')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail()
    .normalizeEmail()
    .isEmail()
    .withMessage((value, { req, path }) => {
      return req.t('validator.email', { field: path });
    })
    .bail()
    .custom(userActive),
];

const emailVerifyValidation = [
  check('email')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail()
    .normalizeEmail()
    .isEmail()
    .withMessage((value, { req, path }) => {
      return req.t('validator.email', { field: path });
    }),

  check('token')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    }),
  // .custom(resetTokenValid),
];

const emailResetValidation = [
  check('token')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail(),
  // .custom(resetTokenValid),

  check('email')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail()
    .normalizeEmail()
    .isEmail()
    .withMessage((value, { req, path }) => {
      return req.t('validator.email', { field: path });
    })
    .custom(userExist),

  check('password')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail()
    .custom(passwordRule),

  check('password_confirmation')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path.replace('_', ' ') });
    })
    .bail()
    .custom(passwordConfirmation),
];

const changePasswordValidation = [
  check('current-password')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path.replace('_', ' ') });
    })
    .bail(),
  // .custom(currentPassword),

  check('password')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail()
    // .custom(cannotSameCurrentPassword)
    .bail()
    .custom(passwordRule),

  check('password_confirmation')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path.replace('_', ' ') });
    })
    .bail()
    .custom(passwordConfirmation),
];

const otpVerifyValidation = [
  check('email')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    })
    .bail()
    .normalizeEmail()
    .isEmail()
    .withMessage((value, { req, path }) => {
      return req.t('validator.email', { field: path });
    }),

  check('otp')
    .notEmpty()
    .withMessage((value, { req, path }) => {
      return req.t('validator.required', { field: path });
    }),
  // .custom(resetOtpValid),
];
export {
  emailNotifValidation,
  emailVerifyValidation,
  emailResetValidation,
  changePasswordValidation,
  otpVerifyValidation,
};
