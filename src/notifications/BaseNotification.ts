import { Message } from 'firebase-admin/lib/messaging/messaging-api';
import url from '../config/url';
import UserService from '../services/UserService';
import { sendFCMParam } from '../@types/common';
// import fireadmin from '../utils/Fireadmin';

abstract class BaseNotification {
  protected _channel: string[];
  constructor(channel: string | string[]) {
    if (typeof channel === 'string') {
      this._channel = [channel];
    } else {
      this._channel = channel;
    }
  }

  abstract toDatabase(): void;
  abstract toMail(): void;
  abstract toFCM(): void;

  async sendFCM(params: sendFCMParam): Promise<void> {
    const user = await UserService.find(params.userId);

    if (user?.fcmToken) {
      const messagenotif: Message = {
        token: user.fcmToken,
        webpush: {
          notification: {
            title: params.title,
            body: params.message,
            icon: `${url.base_url}/img/logo.png`,
            data: {
              url: `/notification-detail/${params.notifId}`,
            },
          },
          fcmOptions: {
            link: `${url.front_end}/notification-detail/${params.notifId}`,
          },
        },
      };
      // fireadmin.messaging().send(messagenotif);
    }
  }

  protected notify() {
    this._channel.forEach((channel) => {
      switch (channel) {
        case 'mail':
          this.toMail();
          break;
        case 'fcm':
          this.toFCM();
          break;
        case 'database':
          this.toDatabase();
          break;
        default:
          break;
      }
    });
  }
}
export default BaseNotification;
