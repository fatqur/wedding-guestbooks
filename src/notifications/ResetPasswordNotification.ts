import BaseNotification from './BaseNotification';
import config from '../config';
import url from '../config/url';
import UserService from '../services/UserService';
import { SendMailParams } from '../@types/common';
import Mail from '../utils/Mail';

class ResetPasswordNotification extends BaseNotification {
  private _email: string;
  private _token: string;
  constructor(email: string, token: string) {
    super('mail');

    this._email = email;
    this._token = token;
    this.notify();
  }

  async toMail(): Promise<void> {
    const user = await UserService.findUserByEmail(this._email);
    let reset_password_link = `${url.front_end.base_url}${url.front_end.reset_password_uri}`;
    reset_password_link += `?email=${this._email}&token=${this._token}`;
    const mailOptions: SendMailParams = {
      to: this._email,
      subject:
        user?.lang === 'id' ? 'Setel Ulang Kata Sandi' : 'Reset Password',
      template: user?.lang === 'id' ? 'resetpassword-id' : 'resetpassword',
      context: {
        title:
          user?.lang === 'id' ? 'Setel Ulang Kata Sandi' : 'Reset Password',
        asset_url: url.base_url,
        name: user?.name,
        link: reset_password_link,
        expired: config.expiration.reset_password,
      },
    };
    Mail.send(mailOptions);
  }

  toDatabase(): void {}
  toFCM(): void {}
}

export default ResetPasswordNotification;
