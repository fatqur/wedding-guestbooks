import BaseNotification from './BaseNotification';
import config from '../config';
import url from '../config/url';
import UserService from '../services/UserService';
import { SendMailParams } from '../@types/common';
import Mail from '../utils/Mail';

class ChangedEmailNotification extends BaseNotification {
  private _email: string;
  private _token: string;
  constructor(email: string, token: string) {
    super('mail');

    this._email = email;
    this._token = token;
    this.notify();
  }

  async toMail(): Promise<void> {
    const user = await UserService.findUserByEmail(this._email);
    let change_email_link = `${url.front_end.base_url}${url.front_end.change_email_uri}`;
    change_email_link += `?email=${this._email}&token=${this._token}`;
    const mailOptions: SendMailParams = {
      to: this._email,
      subject:
        user?.lang === 'id'
          ? 'Ubah Konfirmasi Email'
          : 'Change Email Confirmation',
      template: user?.lang === 'id' ? 'changeemail-id' : 'changeemail',
      context: {
        title:
          user?.lang === 'id'
            ? 'Ubah Konfirmasi Email'
            : 'Change Email Confirmation',
        asset_url: url.base_url,
        name: user?.name,
        link: change_email_link,
        expired: config.expiration.change_email,
      },
    };
    Mail.send(mailOptions);
  }

  toDatabase(): void {}
  toFCM(): void {}
}

export default ChangedEmailNotification;
