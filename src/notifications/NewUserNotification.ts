import BaseNotification from './BaseNotification';
import url from '../config/url';
import config from '../config';
import UserService from '../services/UserService';
import { SendMailParams } from '../@types/common';
import Mail from '../utils/Mail';

class NewUserNotification extends BaseNotification {
  private _email: string;
  private _token: string;
  constructor(email: string, token: string) {
    super('mail');

    this._email = email;
    this._token = token;
    this.notify();
  }

  async toMail(): Promise<void> {
    const user = await UserService.findUserByEmail(this._email, 'unverified');
    let verify_account_link = `${url.front_end.base_url}${url.front_end.verify_account_uri}`;
    verify_account_link += `?email=${this._email}&token=${this._token}`;

    const mailOptions: SendMailParams = {
      to: this._email,
      subject:
        user?.lang === 'id'
          ? 'Pendaftaran berhasil'
          : 'Registered successfully',
      template: user?.lang === 'id' ? 'newuser-id' : 'newuser',
      context: {
        title:
          user?.lang === 'id'
            ? 'Pendaftaran berhasil'
            : 'Registered successfully',
        asset_url: url.base_url,
        name: user?.name,
        link: verify_account_link,
        expired: config.expiration.verify_account / 60,
      },
    };
    Mail.send(mailOptions);
  }

  toDatabase(): void {}
  toFCM(): void {}
}

export default NewUserNotification;
