import { Prisma } from '@prisma/client';
import { PaginationRequest, PaginationResult } from '../@types/pagination';
import Pagination from '../utils/Pagination';
import { prisma } from '../utils/PrismaClient';
import config from '../config';
import { createGuestbook } from '../@types/guestbook';

class GuestbookService {
  public static async getAll(
    params: PaginationRequest,
    isGuest: boolean = true
  ): Promise<PaginationResult> {
    let where: Prisma.GuestbookWhereInput | undefined = params.search
      ? {
          OR: [
            {
              name: {
                contains: params.search.toLowerCase(),
              },
            },
            {
              address: {
                contains: params.search.toLowerCase(),
              },
            },
            {
              phone: {
                contains: params.search.toLowerCase(),
              },
            },
            {
              note: {
                contains: params.search.toLowerCase(),
              },
            },
          ],
        }
      : undefined;
    const limit = params.size ? parseInt(params.size) : config.pagination.size;
    const offset = params.page ? (parseInt(params.page) - 1) * limit : 0;

    const orderDir = params.orderDir || 'asc';
    let order = {};
    switch (params.orderBy) {
      case 'name':
        order = { name: orderDir };
        break;
      case 'address':
        order = { address: orderDir };
        break;
      case 'phone':
        order = { phone: orderDir };
        break;
      case 'note':
        order = { note: orderDir };
        break;
      case 'created':
        order = { createdAt: orderDir };
        break;
      default:
        order = { createdAt: 'desc' };
        break;
    }

    const [total, guestbooks] = await Promise.all([
      prisma.guestbook.count({ where: where }),
      prisma.guestbook.findMany({
        select: isGuest ? { name: true, note: true } : undefined,
        where: where,
        orderBy: order,
        skip: offset,
        take: limit,
      }),
    ]);
    return Pagination.formatResult(params, guestbooks, total);
  }

  public static async show(id: number) {
    const guestbook = await prisma.guestbook.findUnique({ where: { id } });
    return guestbook;
  }

  public static async create(data: createGuestbook) {
    const guestbook = await prisma.guestbook.create({ data });
    return guestbook;
  }

  public static async delete(id: number) {
    const guestbook = await prisma.guestbook.delete({ where: { id } });
    return guestbook;
  }
}
export default GuestbookService;
