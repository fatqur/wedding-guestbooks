import jwt from 'jsonwebtoken';
import config from '../config/index';
import { authUser } from '../@types/auth';
import hash from '../utils/Hash';
import { prisma } from '../utils/PrismaClient';

class AuthService {
  public static async fastCheckAttempt(
    email: string,
    password: string
  ): Promise<boolean> {
    const user = await prisma.user.findUnique({ where: { email } });
    if (user) {
      const match = await hash.compare(password, user.password);
      return match;
    } else {
      return false;
    }
  }
  
  public static async attempt(email: string, password: string, ip?: string) {
    const user = await prisma.user.findUnique({ where: { email } });
    if (!user) {
      return null;
    }
    const match = await hash.compare(password, user.password);
    if (!match) {
      return null;
    }

    // const time = new Date().getTime();
    // const expirationTime = time + Number(config.jwt_expiretime) * 100000;
    // const expirationTimeInSeconds = Math.floor(expirationTime / 1000);

    const authData: authUser = {
      id: user.id,
      name: user.name,
      email: user.email,
      // usertype: user.type,
      // language: user.lang,
    };
    const options: jwt.SignOptions = {
      issuer: config.jwt_issuer,
      algorithm: 'HS256',
      expiresIn: config.jwt_expiretime,
    };

    const token = jwt.sign(authData, config.jwt_secret, options);
    const refresh_token = jwt.sign(authData, config.jwt_secret, options);
    const tokenData = {
      token: token,
      refresh_token: refresh_token,
    };

    // update user
    await prisma.user.update({
      where: { id: user.id },
      data: {
        token: token,
      },
    });
    return tokenData;
  }

  public static async attemptRefresh(id: number) {
    const user = await prisma.user.findUnique({ where: { id } });
    if (!user) {
      return null;
    }

    const time = new Date().getTime();
    const expirationTime = time + Number(config.jwt_expiretime) * 100000;
    const expirationTimeInSeconds = Math.floor(expirationTime / 1000);

    const authData: authUser = {
      id: user.id,
      name: user.name,
      email: user.email,
    };
    const options: jwt.SignOptions = {
      issuer: config.jwt_issuer,
      algorithm: 'HS256',
      expiresIn: expirationTimeInSeconds,
    };

    const token = jwt.sign(authData, config.jwt_secret, options);
    const tokenData = {
      token: token,
    };

    // update user
    await prisma.user.update({
      where: { id: user.id },
      data: {
        token: token,
      },
    });
    return tokenData;
  }

  public static async logout(userid: any) {
    await prisma.user.update({
      where: { id: userid },
      data: { token: null, fcmToken: null },
    });
    return true;
  }

  public static async loggedUser(userid: any) {
    const user = await prisma.user.findUnique({
      where: { id: userid },
      include: {
        roles: {
          include: {
            permissions: true,
          },
        },
        permissions: true,
      },
    });

    let temppermissions: string[] = [];
    let temproles: { name: string; code: string }[] = [];
    user?.permissions.forEach((permission) => {
      temppermissions.push(permission.code);
    });
    user?.roles.forEach((role) => {
      temproles.push({ name: role.name, code: role.code });
      role.permissions.forEach((permission) => {
        if (temppermissions.indexOf(permission.code) == -1) {
          temppermissions.push(permission.code);
        }
      });
    });

    return {
      id: user?.id,
      name: user?.name,
      email: user?.email,
      status: user?.status,
      avatar: user?.avatar,
      roles: temproles,
      permissions: temppermissions,
    };
  }

  public static async validLogin(id: any, token: any) {
    return await prisma.user.findFirst({
      where: {
        id,
        token,
      },
    });
  }

  public static async hasPermission(userid: number, permission: string) {
    const hasPermission = await prisma.user.findFirst({
      select: { id: true },
      where: {
        AND: [
          { id: userid },
          {
            OR: [
              {
                roles: {
                  some: {
                    permissions: {
                      some: {
                        code: permission,
                      },
                    },
                  },
                },
              },
              {
                permissions: {
                  some: {
                    code: permission,
                  },
                },
              },
            ],
          },
        ],
      },
    });
    return hasPermission ? true : false;
  }
}
export default AuthService;
