import AuthRoutes from './AuthRoutes';
import BaseRoutes from './BaseRoutes';
import ProfileRoutes from './ProfileRoutes';
import RoleRoutes from './RoleRoutes';
import GuestbookRoutes from './GuestbookRoutes';

class AppRoute extends BaseRoutes {
  routes(): void {
    //application routes
    this.router.use('/auth', AuthRoutes);
    this.router.use('/profile', ProfileRoutes);
    this.router.use('/roles', RoleRoutes);
    this.router.use('/guestbook', GuestbookRoutes);
  }
}
export default new AppRoute().getRouter();
