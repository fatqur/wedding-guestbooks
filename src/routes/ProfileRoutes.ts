import BaseRoutes from './BaseRoutes';
// import PasswordController from '../controllers/PasswordController';
import ProfileController from '../controllers/ProfileController';
import auth from '../middleware/auth';
import handleValidation from '../middleware/handlevalidation';
import { changePasswordValidation } from '../validators/passwordvalidator';
import {
  avatarValidation,
  changeEmailValidation,
  changeLanguageValidator,
  updateProfileValidation,
} from '../validators/uservalidator';

class ProfileRoutes extends BaseRoutes {
  routes(): void {
    this.router.get('/', auth, ProfileController.index);

    this.router.patch(
      '/',
      auth,
      avatarValidation,
      updateProfileValidation,
      handleValidation,
      ProfileController.update
    );

    this.router.post(
      '/change-language',
      auth,
      changeLanguageValidator,
      handleValidation,
      ProfileController.changeLang
    );

    // this.router.post(
    //   '/change-password',
    //   auth,
    //   changePasswordValidation,
    //   handleValidation,
    //   PasswordController.changePassword
    // );

    this.router.post(
      '/change-email',
      auth,
      changeEmailValidation,
      handleValidation,
      ProfileController.changeEmail
    );
  }
}
export default new ProfileRoutes().getRouter();
