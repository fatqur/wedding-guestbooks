import BaseRoutes from './BaseRoutes';
import RoleController from '../controllers/RoleController';
import auth from '../middleware/auth';
import can from '../middleware/authorize';
import handleValidation from '../middleware/handlevalidation';
import optionalAuth from '../middleware/optionalauth';
import {
  createPermissionValidation,
  createRoleValidation,
  deletePermissionValidation,
  deleteRoleValidation,
  updateRoleValidation,
} from '../validators/rolevalidation';

class RoleRoutes extends BaseRoutes {
  routes(): void {
    // get all roles pagination
    this.router.get('/', auth, can('read_roles'), RoleController.index);
    //get role list
    this.router.get('/list', optionalAuth, RoleController.list);

    // get permission list
    this.router.get(
      '/permissions',
      auth,
      can('read_roles'),
      RoleController.permissions
    );

    // show the role
    this.router.get('/:code', auth, can('read_roles'), RoleController.show);

    // create new role
    this.router.post(
      '/',
      auth,
      can('create_roles'),
      createRoleValidation,
      handleValidation,
      RoleController.create
    );

    // update the role
    this.router.patch(
      '/:code',
      auth,
      can('update_roles'),
      updateRoleValidation,
      handleValidation,
      RoleController.update
    );

    // delete the role
    this.router.delete(
      '/:code',
      auth,
      can('delete_roles'),
      deleteRoleValidation,
      handleValidation,
      RoleController.delete
    );

    // -- Permission: for development purposes only --
    this.router.post(
      '/permissions',
      auth,
      can('create_roles'),
      createPermissionValidation,
      handleValidation,
      RoleController.createPermission
    );
    // delete the role
    this.router.delete(
      '/permissions/:code',
      auth,
      can('delete_roles'),
      deletePermissionValidation,
      handleValidation,
      RoleController.deletePermission
    );
    // -- End Permission --
  }
}
export default new RoleRoutes().getRouter();
