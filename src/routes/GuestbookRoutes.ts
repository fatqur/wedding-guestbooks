import GuestbookController from '../controllers/GuestbookController';
import auth from '../middleware/auth';
import can from '../middleware/authorize';
import handleValidation from '../middleware/handlevalidation';
import optionalAuth from '../middleware/optionalauth';
import { createGuestbookValidation } from '../validators/guestbookvalidator';
import BaseRoutes from './BaseRoutes';

class GuestbookRoutes extends BaseRoutes {
  routes(): void {
    this.router.get(
      '/',
      optionalAuth,
      can('browse_guestbooks'),
      GuestbookController.index
    );
    this.router.post(
      '/',
      optionalAuth,
      can('add_guestbooks'),
      createGuestbookValidation,
      handleValidation,
      GuestbookController.create
    );
    this.router.get(
      '/:id',
      auth,
      can('read_guestbooks'),
      GuestbookController.show
    );
    this.router.delete(
      '/:id',
      auth,
      can('delete_guestbooks'),
      GuestbookController.delete
    );
  }
}
export default new GuestbookRoutes().getRouter();
