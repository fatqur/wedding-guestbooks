import BaseRoutes from './BaseRoutes';
import AuthController from '../controllers/AuthController';
import auth from '../middleware/auth';
import handleValidation from '../middleware/handlevalidation';
import loginLimiter from '../middleware/loginlimiter';
import {
  accountVerifyValidation,
  changeEmailVerifyValidation,
  loginValidation,
  setTokenValidation,
} from '../validators/authvalidator';

class AuthRoutes extends BaseRoutes {
  routes(): void {
    this.router.post(
      '/login',
      loginValidation,
      loginLimiter,
      handleValidation,
      AuthController.login
    );

    this.router.get('/user', auth, AuthController.user);

    this.router.post('/logout', auth, AuthController.logout);

    this.router.post(
      '/verify',
      accountVerifyValidation,
      handleValidation,
      AuthController.verify
    );

    this.router.get('/refresh-token', AuthController.refreshToken);

    // this.router.post(
    //   '/verify-email',
    //   changeEmailVerifyValidation,
    //   handleValidation,
    //   AuthController.verifyEmail
    // );

    this.router.post(
      '/set-fcm-token',
      auth,
      setTokenValidation,
      handleValidation,
      AuthController.setFCMtoken
    );
  }
}
export default new AuthRoutes().getRouter();
