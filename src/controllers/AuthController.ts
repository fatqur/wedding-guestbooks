import jwt, { JwtPayload } from 'jsonwebtoken';
import { CookieOptions, Request, Response } from 'express';
import BaseController from './BaseController';
import AuthService from '../services/AuthService';
import config from '../config';
import UserService from '../services/UserService';

class AuthController extends BaseController {
  public login = async (req: Request, res: Response) => {
    try {
      const params = req.body as {
        email: string;
        password: string;
      };
      const token = await AuthService.attempt(
        params.email,
        params.password,
        req.ip
      );
      return token
        ? this.success(res, req.t('auth.loginsuccess'), { token })
        : this.unprocessableEntity(res, {
            errors: {
              email: 'The provided credentials are incorrect.',
            },
          });
    } catch (error) {
      return this.error(res, error, req.t('response.error'));
    }

    //   // test
    //   const accessTokenCookieOptions: CookieOptions = {
    //     expires: new Date(Date.now() + 3 * 60 * 60),
    //     maxAge: 3 * 60 * 60,
    //     httpOnly: true,
    //   };

    //   const refreshTokenCookieOptions: CookieOptions = {
    //     expires: new Date(Date.now() + 3 * 60 * 60),
    //     maxAge: 3 * 60 * 60,
    //     httpOnly: true,
    //   };

    //   const tokenData = await AuthService.attempt(
    //     params.email,
    //     params.password,
    //     req.ip
    //   );

    //   res.cookie('access_token', tokenData?.token, accessTokenCookieOptions);
    //   res.cookie(
    //     'refresh_token',
    //     tokenData?.refresh_token,
    //     refreshTokenCookieOptions
    //   );
    //   res.cookie('logged_in', true, {
    //     ...accessTokenCookieOptions,
    //     httpOnly: false,
    //   });

    //   // return token
    //   //   ? this.success(res, req.t('auth.loginsuccess'), { token })
    //   //   : this.unprocessableEntity(res, {
    //   //       errors: [
    //   //         {
    //   //           msg: req.t('auth.incorrect'),
    //   //           param: 'email',
    //   //           location: 'body',
    //   //         },
    //   //       ],
    //   //     });
    //   return tokenData
    //     ? this.success(res, req.t('auth.loginsuccess'), {
    //         token: tokenData.token,
    //       })
    //     : this.unprocessableEntity(res, {
    //         errors: {
    //           email: 'The provided credentials are incorrect.',
    //         },
    //       });
    // } catch (error: any) {
    //   return this.error(res, error, req.t('response.error'));
    // }
  };

  public refreshToken = async (req: Request, res: Response) => {
    try {
      const { refresh_token } = req.cookies as { refresh_token: string };
      const { id } = jwt.verify(refresh_token, config.jwt_secret) as JwtPayload;

      // test
      const accessTokenCookieOptions: CookieOptions = {
        expires: new Date(Date.now() + 3 * 60 * 60),
        maxAge: 3 * 60 * 60,
        httpOnly: true,
      };

      const tokenData = await AuthService.attemptRefresh(id);

      res.cookie('access_token', tokenData?.token, accessTokenCookieOptions);
      res.cookie('logged_in', true, {
        ...accessTokenCookieOptions,
        httpOnly: false,
      });

      return tokenData
        ? this.success(res, req.t('auth.loginsuccess'), {
            token: tokenData.token,
          })
        : this.unprocessableEntity(res, {
            errors: {
              email: 'The provided credentials are incorrect.',
            },
          });
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };

  public logout = async (req: Request, res: Response) => {
    await AuthService.logout(req.app.locals.user.id)
      .then(() => {
        return this.success(res, req.t('auth.logoutsuccess'));
      })
      .catch((error) => {
        return this.error(res, error, req.t('response.error'));
      });
  };

  public user = async (req: Request, res: Response) => {
    try {
      const user = await AuthService.loggedUser(req.app.locals.user.id);
      return this.success(
        res,
        req.t('response.getdata', {
          item: req.language === 'id' ? 'pengguna' : 'user',
        }),
        { user }
      );
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };

  public verify = async (req: Request, res: Response) => {
    try {
      const user = await UserService.activate(req.body.email);
      // this.lastActivity(user.id);
      return this.success(res, req.t('auth.verified'), null);
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };

  // public verifyEmail = async (req: Request, res: Response) => {
  //   try {
  //     const { email, token } = req.body as { email: string; token: string };
  //     const userOldData = await UserService.findUserChangeEmail(email, token);
  //     if (userOldData) {
  //       await UserService.changeEmail(userOldData.id, email);
  //       await UserService.deletTokenEmail(email);
  //       await AuthService.logout(userOldData.id);

  //       return this.success(res, req.t('auth.email.changed'), null);
  //     } else {
  //       return this.notFound(res, req.t('response.notfound'));
  //     }
  //   } catch (error: any) {
  //     return this.error(res, error, req.t('response.error'));
  //   }
  // };

  public setFCMtoken = async (req: Request, res: Response) => {
    try {
      const userId = req.app.locals.user.id;
      const { token } = req.body as { token: string };
      const user = await UserService.setFCMToken(userId, token);

      return this.success(res, 'success set token', null);
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };
}

export default new AuthController();
