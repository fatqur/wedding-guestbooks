import { Request, Response } from 'express';
import BaseController from './BaseController';
import { PaginationRequest } from '../@types/pagination';
import { getPathUrl } from '../utils/General';
import GuestbookService from '../services/GuestbookService';
import { createGuestbook } from '../@types/guestbook';

class GuestbookController extends BaseController {
  public index = async (req: Request, res: Response) => {
    try {
      const params = req.query as PaginationRequest;
      params.path = getPathUrl(req);
      const guestbooks = await GuestbookService.getAll(
        params,
        req.app.locals.user.id ? false : true
      );

      return this.success(
        res,
        req.t('response.getdata', {
          item: req.language === 'id' ? 'buku tamu' : 'guestbooks',
        }),
        guestbooks
      );
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };

  public show = async (req: Request, res: Response) => {
    try {
      const { id } = req.params as { id: string };
      const guestbook = await GuestbookService.show(Number(id));

      return guestbook
        ? this.success(
            res,
            req.t('response.getdata', {
              item: req.language === 'id' ? 'buku tamu' : 'guestbooks',
            }),
            guestbook
          )
        : this.notFound(res, req.t('response.notfound'));
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };

  public create = async (req: Request, res: Response) => {
    try {
      const params = req.body as createGuestbook;
      const guestbook = await GuestbookService.create(params);
      return this.created(
        res,
        req.t('response.created', {
          item: req.language === 'id' ? 'buku tamu' : 'guestbooks',
        }),
        guestbook
      );
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };

  public delete = async (req: Request, res: Response) => {
    try {
      const { id } = req.params as { id: string };
      const guestbook = await GuestbookService.delete(Number(id));

      return guestbook
        ? this.success(
            res,
            req.t('response.delete', {
              item: req.language === 'id' ? 'buku tamu' : 'guestbooks',
            }),
            guestbook
          )
        : this.notFound(res, req.t('response.notfound'));
    } catch (error: any) {
      return this.error(res, error, req.t('response.error'));
    }
  };
}
export default new GuestbookController();
