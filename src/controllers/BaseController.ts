import { Response } from 'express';
import Logger from '../utils/Logger';
import config from '../config/index';
import UserService from '../services/UserService';

abstract class BaseController {
  protected success<T>(res: Response, message: string, data?: T): Response {
    return res.status(200).json({ status: true, message, data });
  }

  protected created<T>(res: Response, message: string, data?: T): Response {
    return res.status(201).json({ status: true, message, data });
  }

  protected error(
    res: Response,
    errors: any,
    message?: string
  ): Response {
    let responseData: {
      status: boolean;
      message: string;
      errors?: any;
    } = {
      status: false,
      message: message ?? 'An error occured! Please contact your administrator',
    };
    Logger.error(errors);
    return res.status(500).json(responseData);
  }

  protected unprocessableEntity(
    res: Response,
    errorResult: any,
    message?: string
  ): Response {
    return res.status(422).json({
      status: false,
      message: message ?? 'The given data was invalid',
      errors: errorResult.errors,
    });
  }

  protected unauthorized(res: Response, message?: string): Response {
    return res
      .status(401)
      .json({ status: false, message: message ?? 'Unauthorized' });
  }

  protected forbidden(res: Response, message?: string): Response {
    return res
      .status(403)
      .json({ status: false, message: message ?? 'Forbidden' });
  }

  protected notFound(res: Response, message?: string): Response {
    return res
      .status(404)
      .json({ status: false, message: message ?? 'Not Found' });
  }

  protected tooMany(res: Response, message?: string): Response {
    return res
      .status(429)
      .json({ status: false, message: message ?? 'Too many requests' });
  }

  protected methodNotAllowed(res: Response, message?: string): Response {
    return res
      .status(405)
      .json({ status: false, message: message ?? 'Method Not Allowed' });
  }

  protected paymentRequired(res: Response, message?: string): Response {
    return res
      .status(402)
      .json({ status: false, message: message ?? 'Payment Required' });
  }

  protected conflict(res: Response, message?: string): Response {
    return res
      .status(409)
      .json({ status: false, message: message ?? 'Conflict' });
  }

  // protected async lastActivity(id: number) {
  //   const activity = await UserService.lastActivity(id);
  //   return activity;
  // }
}

export default BaseController;
