export interface authUser {
  id: number;
  name: string;
  email: string;
}