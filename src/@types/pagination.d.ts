enum OrderDir {
  'asc',
  'desc',
}

export interface PaginationRequest {
  page?: string;
  size?: string;
  orderBy?: string;
  orderDir?: OrderDir;
  search?: string;
  path?: string;
}

export interface PaginationResult {
  rows: any[];
  meta: {
    total: number;
    size: number;
    current_page: number;
    last_page: number;
    from: number;
    to: number;
    search: string | null;
    path?: string;
    first_page_url?: string | null;
    last_page_url?: string | null;
    next_page_url?: string | null;
    prev_page_url?: string | null;
  };
}

export interface patientListRequest extends PaginationRequest {
  type?: string;
  status?: string;
  medicalId?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  placeOfBirth?: string;
  dateOfBirth?: string;
  address?: string;
  identityTypeCode?: string;
  identityNumber?: string;
  showOnlyLatest?: string;
}

export interface officerListRequest extends PaginationRequest {
  status?: string;
  role?: string;
}

export interface medicineListRequest extends PaginationRequest {
  name?: string;
  category?: string;
}

export interface programListRequest extends PaginationRequest {
  category?: string;
}

export interface treatmentListRequest extends PaginationRequest {
  branch?: string;
}

export interface prescriptionListRequest extends PaginationRequest {
  medicalId?: string;
  date?: string;
  patientName?: string;
  status?: string;
  practicianName?: string;
}

export interface prescriptionItemListRequest extends PaginationRequest {
  name?: string;
  qty?: string;
  category?: string;
  dosage?: string;
  dose?: string;
}

export interface registrationListRequest extends PaginationRequest {
  date?: string;
  chargeCode?: string;
  chargeParent?: string;
}

export interface appointmentListRequest extends PaginationRequest {
  status?: string;
  medicalId?: string;
  practicianId?: string;
  date?: string;
}

export interface feedbackListRequest extends PaginationRequest {
  refer?: string;
  rate?: string;
}
export interface activityLogListRequest extends PaginationRequest {
  subject?: string;
  causer?: string;
  startdate?: string;
  enddate?: string;
}
export interface onLeaveRequest extends PaginationRequest {
  date?: string;
  type?: string;
  status?: string;
  officer?: string;
}

export interface notificationRequest extends PaginationRequest {
  readStatus?: string;
}

export interface labTrackingRequest extends PaginationRequest {
  labId?: string;
  status?: string;
  date?: string;
}

export interface orderRequest extends PaginationRequest {
  date?: string;
  medicalId?: string;
  payerType?: string;
}

export interface sampleLabRequest extends PaginationRequest {
  type?: string;
  vendor?: string;
  status?: string;
}

export interface aboutRequest extends PaginationRequest {
  label?: string;
  status?: string;
}

export interface faqRequest extends PaginationRequest {
  question?: string;
  answer?: string;
  status?: string;
}

export interface beauticianRequest extends PaginationRequest {
  patient?: string;
  date?: string;
  doctor?: string;
  beautician?: string;
  treatment?: string;
  role?: string;
}
