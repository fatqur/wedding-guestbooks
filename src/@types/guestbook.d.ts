export interface createGuestbook {
  name: string;
  address: string;
  phone: string;
  note: string;
}
