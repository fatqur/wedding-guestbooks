export type roleReqBody = {
  name: string;
  permissions: string[];
};

export type roleReqParams = {
  code: string;
};
