import dotenv from 'dotenv';

dotenv.config();
export default {
  app_name: process.env.APP_NAME || 'Fire Express',
  node_env: process.env.NODE_ENV || 'production',
  is_production: process.env.NODE_ENV === 'production',
  
  port: parseInt(process.env.PORT || '3000', 10),
  timezone: process.env.TZ || 'Asia/Jakarta',
  start_jobscheduler: process.env.JOBSCHEDULER || 'false',

  jwt_expiretime: process.env.APP_JWT_EXPIRETIME || '1d',
  jwt_issuer: process.env.APP_JWT_ISSUER || 'fire-express',
  jwt_secret: process.env.APP_JWT_SECRET || "1d^5enek(8o-+[31",
  
  pagination: {
    size: 10,
  },
  expiration: {
    reset_password: 60, // 60 minutes
    change_email: 60, // 60 minutes
    verify_account: 24 * 60, // 24 hours = 1440 minutes
  },
  role: {
    immutable: ['superadmin'],
    superadmin: 'superadmin',
  },
};
