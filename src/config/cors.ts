import { CorsOptions } from 'cors';
import dotenv from 'dotenv';

dotenv.config();
const whitelist: string[] = process.env.ALLOW_ORIGINS
  ? process.env.ALLOW_ORIGINS.split(',')
  : [];

const corsOptions: CorsOptions = {
  origin: whitelist,
  optionsSuccessStatus: 200,
};

export default corsOptions;
