import dotenv from 'dotenv';

dotenv.config();
export default {
  base_url: process.env.BASE_URL || 'http://localhost:3000',
  front_end: {
    base_url: process.env.FRONT_URL || 'http://localhost:3000',
    reset_password_uri: '/verify-password',
    change_email_uri: '/verify-email-changed',
    verify_account_uri: '/verify-email',
  },
};
