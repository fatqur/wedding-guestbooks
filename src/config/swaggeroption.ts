import path from 'path';

const swagger_path = path.resolve(
  __dirname,
  '..',
  '..',
  'docs',
  '**',
  '*.yaml'
);

const swaggeroption = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Fire Express Documentation',
      description:
        "This is API documentation for Fire Express. If there's anything else you need, feel free to ask. Good luck with your project!",
      contact: {
        name: 'Ro',
        email: 'fatqur.fr@gmail.com',
      },
      version: '1.0.0',
    },
    servers: [
      {
        url: 'https://localhost:3000/api',
      }
    ],
  },
  components: {
    schemas: {
      GeneralError: {
        type: 'object',
        properties: {
          code: {
            type: 'integer',
            format: 'int32',
          },
          message: {
            type: 'string',
          },
        },
      },
    },
    parameters: {
      skipParam: {
        name: 'skip',
        in: 'query',
        description: 'number of items to skip',
        required: true,
        schema: {
          type: 'integer',
          format: 'int32',
        },
      },
      limitParam: {
        name: 'limit',
        in: 'query',
        description: 'max records to return',
        required: true,
        schema: {
          type: 'integer',
          format: 'int32',
        },
      },
    },
    responses: {
      NotFound: {
        description: 'Entity not found.',
      },
      IllegalInput: {
        description: 'Illegal input for operation.',
      },
      GeneralError: {
        description: 'General Error',
        content: {
          'application/json': {
            schema: {
              $ref: '#/components/schemas/GeneralError',
            },
          },
        },
      },
    },
    securitySchemes: {
      api_key: {
        type: 'apiKey',
        name: 'api_key',
        in: 'header',
      },
    },
  },
  apis: [swagger_path],
};
export default swaggeroption;
