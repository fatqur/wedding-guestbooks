import { Request, Response, NextFunction } from 'express';
import { validationResult } from 'express-validator';
import ResponseController from '../controllers/ResponseController';

const handleValidation = (req: Request, res: Response, next: NextFunction) => {
  const errorResult = validationResult(req);
  if (!errorResult.isEmpty()) {
    return ResponseController.unprocessableEntity(
      res,
      errorResult,
      req.t('response.unprocessableEntity')
    );
  } else {
    next();
  }
};
export default handleValidation;
