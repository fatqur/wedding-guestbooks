import { Request, Response, NextFunction } from 'express';
import { authPartner } from '../@types/auth';
import responseController from '../controllers/ResponseController';
import AuthService from '../services/AuthService';

const partnerCan = (permission: string) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    const { id } = req.app.locals.partner as authPartner;
    // const hasPermission = await AuthService.hasPermission(id, permission);
    // if (hasPermission) {
    //   next();
    // } else {
    //   return responseController.forbidden(res);
    // }
  };
};

export default partnerCan;
