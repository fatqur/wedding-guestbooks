import { Request, Response, NextFunction } from 'express';
import { authUser } from '../@types/auth';
import responseController from '../controllers/ResponseController';
import AuthService from '../services/AuthService';

const can = (permission: string) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    if (req.app.locals.user.id) {
      const { id } = req.app.locals.user as authUser;
      const hasPermission = await AuthService.hasPermission(id, permission);
      if (hasPermission) {
        next();
      } else {
        return responseController.forbidden(res);
      }
    } else {
      next();
    }
  };
};

export default can;
