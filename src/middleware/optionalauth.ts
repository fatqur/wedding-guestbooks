import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import config from '../config/index';
import responseController from '../controllers/ResponseController';
import authService from '../services/AuthService';
import { authUser } from '../@types/auth';
import Logger from '../utils/Logger';

const optionalAuth = (req: Request, res: Response, next: NextFunction) => {
  const token = req.headers.authorization?.split(' ')[1];
  if (token) {
    jwt.verify(token, config.jwt_secret, async (error, decoded) => {
      if (error) {
        return responseController.unauthorized(res, req.t('auth.invalid'));
      } else {
        const userData = decoded as authUser;
        await authService
          .validLogin(userData.id, token)
          .then((result) => {
            if (result) {
              req.app.locals.user = userData;
              next();
            } else {
              return responseController.unauthorized(
                res,
                req.t('auth.invalid')
              );
            }
          })
          .catch((err) => {
            Logger.error(err);
          });
      }
    });
  } else {
    const userData = {
      id: null,
      name: null,
      email: null,
    };
    req.app.locals.user = userData;
    next();
  }
};

export default optionalAuth;
