import http from 'http';
import Cron from '../utils/Cronjob';
import app from '../app';
import config from '../config/index';

class ExpressServer {
  private port: number | string | boolean;
  private server: http.Server;

  constructor() {
    this.port = this.normalizePort(config.port);
    app.set('port', this.port);
    this.server = http.createServer(app);
  }

  public start() {
    this.server.listen(this.port, () => {
      console.log('Listening on port ' + this.port);
    });

    this.server.on('error', this.onError);

    if (config.start_jobscheduler === 'true') {
      console.log('start scheduler : ' + config.start_jobscheduler);
      Cron.initScheduller();
    }
  }

  private normalizePort(val: any): number | string | boolean {
    const port = parseInt(val, 10);

    if (isNaN(port)) {
      return val;
    }

    if (port >= 0) {
      return port;
    }

    return false;
  }

  private onError = (error: { syscall: string; code: any }) => {
    if (error.syscall !== 'listen') {
      throw error;
    }

    const bind =
      typeof this.port === 'string' ? 'Pipe ' + this.port : 'Port ' + this.port;

    switch (error.code) {
      case 'EACCES':
        console.log(bind + ' requires elevated privileges');
        process.exit(1);
        break;
      case 'EADDRINUSE':
        console.log(bind + ' is already in use');
        process.exit(1);
        break;
      case 'ECONNREFUSED':
        console.log(error);
        process.exit(1);
        break;
      default:
        throw error;
    }
  };
}

new ExpressServer().start();
