import compression from 'compression';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express, { Application, Request, Response, NextFunction } from 'express';
import helmet from 'helmet';
import path from 'path';
import favicon from 'serve-favicon';
// swagger
import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';

//import configuration
import corsOptions from './config/cors';
import swaggerOption from './config/swaggeroption';

// import middleware
import morganMiddleware from './middleware/morgan';
import { i18next, i18nextMiddleware } from './middleware/i18next';

// utils
import Logger from './utils/Logger';

import appRoute from './routes';

class ExpressApp {
  private app: Application;

  constructor() {
    this.app = express();

    this.initMiddleware();
    this.initRoutes();
    this.errorHandlers();
  }

  public getApp(): Application {
    return this.app;
  }

  private initMiddleware(): void {
    this.app.set('trust proxy', true);

    this.app.use(compression());
    this.app.use(helmet());
    this.app.use(morganMiddleware);
    this.app.use(cors(corsOptions));
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: false }));
    this.app.use(cookieParser());
    this.app.use(i18nextMiddleware.handle(i18next));

    this.app.use(
      '/assets',
      express.static(path.join(__dirname, '..', 'public'))
    );
    this.app.use(
      '/storages',
      express.static(path.join(__dirname, '..', 'storage', 'uploads'))
    );
    this.app.use(favicon(path.join(__dirname, '..', 'public', 'favicon.png')));
  }

  private initRoutes(): void {
    // documentation
    const spec = swaggerJSDoc(swaggerOption);
    this.app.use(
      '/api-docs',
      swaggerUi.serve,
      swaggerUi.setup(spec, {
        customCss: '.swagger-ui .topbar { display: none }',
        customSiteTitle: 'Fire Express Documentation',
        customfavIcon: '/img/fire-express.png',
      })
    );

    this.app.route('/').get((req, res, next) => {
      res.status(200).json({ status: true, message: req.t('welcome') });
    });

    // App Routes
    this.app.use('/api', appRoute);

    // catch 404 and forward to error handler
    this.app.use(function (req, res, next) {
      res
        .status(404)
        .json({ status: false, message: req.t('response.notfound') });
    });
  }

  private errorHandlers(): void {
    this.app.use(function (
      err: any,
      req: Request,
      res: Response,
      next: NextFunction
    ) {
      console.error(err.stack);
      Logger.error(err.stack);
      return res.status(500).json({
        status: false,
        message: req.t('response.error'),
      });
    });
  }
}

export default new ExpressApp().getApp();
