import path from 'path';
import winston from 'winston';

const storagedir = path.resolve(__dirname, '..', '..', 'storage');
const colors = {
  error: 'red',
  warn: 'yellow',
  info: 'green',
  http: 'magenta',
  debug: 'white',
};

winston.addColors(colors);

const format = winston.format.combine(
  winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.ms' }),
  winston.format.colorize({ all: true }),
  winston.format.printf(
    (info) => `${info.timestamp} ${info.level}: ${info.message}`
  )
);

const transports = [
  new winston.transports.File({
    level: 'error',
    filename: path.resolve(storagedir, 'logs', 'error.log'),
  }),

  new winston.transports.File({
    level: 'debug',
    filename: path.resolve(storagedir, 'logs', 'debug.log'),
  }),

  new winston.transports.File({
    filename: path.resolve(storagedir, 'logs', 'app.log'),
  }),
];

const exceptionHandlers = [
  new winston.transports.File({
    filename: path.resolve(storagedir, 'logs', 'exceptions.log'),
  })
];

const rejectionHandlers = [
  new winston.transports.File({
    filename: path.resolve(storagedir, 'logs', 'rejections.log'),
  })
];

const Logger = winston.createLogger({
  format,
  transports,
  exceptionHandlers,
  rejectionHandlers
});

export default Logger;
