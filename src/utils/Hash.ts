import bcrypt from 'bcrypt';

class Hash {
  private static saltRounds = 12;

  public static hash = async (plaintext: string) => {
    return bcrypt.hashSync(plaintext, this.saltRounds);
  };

  public static compare = async (plaintext: string, hash: string) => {
    return bcrypt.compareSync(plaintext, hash);
  };
}
export default Hash;
