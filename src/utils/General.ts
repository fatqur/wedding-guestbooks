import { Request } from 'express';

export function getPathUrl(req: Request) {
  var oriUrl = req.originalUrl.split('?').shift();
  var forcehttps = req.protocol === 'http' ? 'https' : req.protocol;
  if (oriUrl) {
    if (oriUrl.charAt(0) === '/') {
      oriUrl = oriUrl.substring(1);
    }
  }
  return `${oriUrl}`;
}

export function convertNameToCode(name: string) {
  return name.toLowerCase().replace(' ', '_');
}