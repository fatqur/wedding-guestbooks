import config from '../config';
import { PaginationRequest, PaginationResult } from '../@types/pagination';

class Pagination {
  public static formatResult = (
    params: PaginationRequest,
    data: any,
    total: number,
    filterquery?: string
  ): PaginationResult => {
    const size = params.size ? parseInt(params.size) : config.pagination.size;
    const offset = params.page ? (parseInt(params.page) - 1) * size : 0;

    const last_page = Math.ceil(total / size);
    const curr_page = params.page ? parseInt(params.page) : 1;

    let result: PaginationResult = {
      rows: data,
      meta: {
        total,
        size,
        current_page: curr_page,
        last_page,
        from: offset + 1,
        to: curr_page * size > total ? total : curr_page * size,
        search: params.search || null,
      },
    };

    if (params.path) {
      result.meta.path = params.path;
      result.meta.first_page_url = this.getPaginateLink(
        1,
        size,
        params,
        filterquery
      );
      result.meta.last_page_url = this.getPaginateLink(
        last_page,
        size,
        params,
        filterquery
      );
      if (curr_page == 1) {
        result.meta.prev_page_url = null;
        result.meta.next_page_url =
          last_page > 1
            ? this.getPaginateLink(2, size, params, filterquery)
            : null;
      } else {
        result.meta.prev_page_url = this.getPaginateLink(
          curr_page - 1,
          size,
          params,
          filterquery
        );

        result.meta.next_page_url =
          curr_page !== last_page
            ? this.getPaginateLink(curr_page + 1, size, params, filterquery)
            : null;
      }
    }

    return result;
  };

  private static getPaginateLink(
    page: number,
    size: number,
    params: PaginationRequest,
    filterquery?: string
  ) {
    const querySearch = params.search ? `&search=${params.search}` : '';
    const queryOrderBy = params.orderBy ? `&orderBy=${params.orderBy}` : '';
    const queryOrderDir = params.orderDir ? `&orderDir=${params.orderDir}` : '';
    let link = `${params.path}?`;
    if (filterquery) {
      link += filterquery + '&';
    }
    return `${link}page=${page}&size=${size}${querySearch}${queryOrderBy}${queryOrderDir}`;
  }
}
export default Pagination;
