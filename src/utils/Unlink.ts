import { unlink } from 'fs';
import path from 'path';
import Logger from './Logger';

export function unlinkAvatar(name: string) {
  const file = path.join(__dirname, '..', '..', 'storage', 'uploads', name);
  unlink(file, (err) => {
    if (err) {
      Logger.error(err);
    }
  });
}
