import multer from 'multer';
import path from 'path';

export function singleImageUpload(
  dest: string,
  field_name: string,
  limit: number
) {
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.join(__dirname, '..', '..', 'storage', 'uploads', dest));
    },

    filename: function (req, file, cb) {
      const ext = path.extname(file.originalname);
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
      cb(null, file.fieldname + '-' + uniqueSuffix + ext);
    },
  });

  const upload = multer({
    limits: { fileSize: limit * 1024 },
    fileFilter: (req, file, cb) => {
      const mimeImage = ['image/png', 'image/jpeg', 'image/jpg'];
      if (mimeImage.includes(file.mimetype)) {
        cb(null, true);
      } else {
        cb(new multer.MulterError('LIMIT_UNEXPECTED_FILE'));
      }
    },
    storage: storage,
  });
  return upload.single(field_name);
}

export function multiImageUpload(
  dest: string,
  field_name: string,
  limit: number,
  maxFiles: number
) {
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.join(__dirname, '..', '..', 'storage', 'uploads', dest));
    },

    filename: function (req, file, cb) {
      const ext = path.extname(file.originalname);
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
      cb(null, file.fieldname + '-' + uniqueSuffix + ext);
    },
  });

  const upload = multer({
    limits: { fileSize: limit * 1024 },
    fileFilter: (req, file, cb) => {
      console.log(file);
      const mimeImage = ['image/png', 'image/jpeg', 'image/jpg'];
      if (mimeImage.includes(file.mimetype)) {
        cb(null, true);
      } else {
        cb(new multer.MulterError('LIMIT_UNEXPECTED_FILE'));
      }
    },
    storage: storage,
  });
  return upload.array(field_name, maxFiles);
}

export function singlePdfUpload(
  dest: string,
  field_name: string,
  limit: number
) {
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.join(__dirname, '..', '..', 'storage', 'uploads', dest));
    },

    filename: function (req, file, cb) {
      const ext = path.extname(file.originalname);
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
      cb(null, file.fieldname + '-' + uniqueSuffix + ext);
    },
  });

  const upload = multer({
    limits: { fileSize: limit * 1024 },
    fileFilter: (req, file, cb) => {
      const mimeImage = ['application/pdf'];
      if (mimeImage.includes(file.mimetype)) {
        cb(null, true);
      } else {
        cb(new multer.MulterError('LIMIT_UNEXPECTED_FILE'));
      }
    },
    storage: storage,
  });
  return upload.single(field_name);
}

export function singleFileUpload(
  dest: string,
  field_name: string,
  limit: number,
  mimeFile: string[],
){
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.join(__dirname, '..', '..', 'storage', 'uploads', dest));
    },

    filename: function (req, file, cb) {
      const ext = path.extname(file.originalname);
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
      cb(null, file.fieldname + '-' + uniqueSuffix + ext);
    },
  });

  const upload = multer({
    limits: { fileSize: limit * 1024 },
    fileFilter: (req, file, cb) => {
      if (mimeFile.includes(file.mimetype)) {
        cb(null, true);
      } else {
        cb(new multer.MulterError('LIMIT_UNEXPECTED_FILE'));
      }
    },
    storage: storage,
  });
  return upload.single(field_name);
}